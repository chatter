using System;
using NDesk.DBus;

namespace Telepathy
{
	public class ConnectionManager
	{		
		public const string JabberService = "org.freedesktop.Telepathy.ConnectionManager.gabble";
		public const string JabberPath = "/org/freedesktop/Telepathy/ConnectionManager/gabble";
		
		public const string MsnService = "org.freedesktop.Telepathy.ConnectionManager.msn";
		public const string MsnPath = "/org/freedesktop/Telepathy/ConnectionManager/msn";
		
		public static IConnectionManager FromProtocol (string protocol)
		{
			BusG.Init ();
			Connection conn = NDesk.DBus.Bus.Session;
			
			if (protocol == "jabber" || protocol == "google-talk")
				return conn.GetObject<IConnectionManager> (JabberService, new ObjectPath (JabberPath));
			
			if (protocol == "msn")
				return conn.GetObject<IConnectionManager> (MsnService, new ObjectPath (MsnPath));
			
			throw new Exception ("Unsupported protocol");
		}
	}
	public static class PresenceStr
	{
		public const string Available = "available";
		public const string Brb = "brb";
		public const string Busy = "busy";
		public const string Dnd = "dnd";
		public const string ExtendedAway = "xa";
		public const string Hidden = "hidden";
		public const string Offline = "offline";
	}


}
