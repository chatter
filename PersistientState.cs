using System;
using System.IO;
using System.Xml.Serialization;

namespace Chatter
{
	public delegate void StateCommitHandler (object obj, bool now);
	
	[XmlRootAttribute ("State")]
	public class PersistientState
	{		
		uint timeout_id;
		AccountStore account_store = new AccountStore ();
		
		public PersistientState () { }
		
		public static PersistientState FromNew ()
		{
			PersistientState state = new PersistientState ();
			state.account_store.CommitRequired += state.CommitRequired;
			return state;
		}
		
		public static PersistientState FromStream (Stream stream)
		{
			XmlSerializer serializer = new XmlSerializer (typeof (PersistientState));
			PersistientState state = (PersistientState) serializer.Deserialize (stream);
			state.account_store.CommitRequired += state.CommitRequired;
			state.account_store.LinkAccounts ();
			return state;
		}
		
		public void CommitRequired (object obj, bool now)
		{
			if (timeout_id > 0) {
				GLib.Source.Remove (timeout_id);
				timeout_id = 0;
			}
				
			if (now)
				Serialize ();
			else
				timeout_id = GLib.Timeout.Add (10 * 1000, Timeout);
		}

		//Forget any timeouts and commit if required
		public void OutOfTime ()
		{
			if (timeout_id > 0) {
				GLib.Source.Remove (timeout_id);
				Serialize ();
			}
		}
		
		public bool Timeout ()
		{
			Serialize ();
			return false;
		}

		public void Serialize ()
		{
			Global.Logger.Message ("Writing config file at {0}", Global.Paths.ConfigFile);
			try {
				if (!File.Exists (Global.Paths.ConfigFile)) {
					if (!Directory.Exists (Global.Paths.ConfigDir))
						Directory.CreateDirectory (Global.Paths.ConfigDir);
				}
					
				XmlSerializer serializer = new XmlSerializer (typeof (PersistientState));
				//serializer.UnknownNode += new XmlNodeEventHandler (SerializerUnknownNode);
      			//serializer.UnknownAttribute += new XmlAttributeEventHandler (SerializerUnknownAttribute);
      
				TextWriter writer = new StreamWriter (Global.Paths.ConfigFile);
				serializer.Serialize (writer, this);
				writer.Close ();
			} catch (Exception e) {
				Global.Logger.Error ("Failed to write application state: {0}", e.Message);
			}
		}
		
		[XmlElement ("AccountStore")]
		public AccountStore AccountStore
		{ 
			get { return account_store; }
			set { account_store = value; }
		}
		
		/*
		protected void SerializerUnknownNode (object sender, XmlNodeEventArgs e)
		{
			Console.WriteLine("Unknown Node:" +   e.Name + "\t" + e.Text);
		}

		protected void SerializerUnknownAttribute (object sender, XmlAttributeEventArgs e)
		{
			System.Xml.XmlAttribute attr = e.Attr;
			Console.WriteLine("Unknown attribute " + attr.Name + "='" + attr.Value + "'");
		}
		*/
	}
}
