using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;

namespace Chatter.Plugin
{
	public class FakePassword : IPassword
	{
		public string GetPassword (string id)
		{
			return "hi";
		}
		
		public void SetPassword (string id, string password) { }
		
		public void Activate () { }
		public void Deactivate () {}
	}
	
	// Generic plugin interface
	//
	public interface IPlugin
	{
		void Activate ();
		void Deactivate ();
	}

	// Plugin to provide secure passwords to chatter
	//
	public interface IPassword : IPlugin
	{
		string GetPassword (string id);
		void SetPassword (string id, string password);
	}
	
	public class PluginManager
	{
		List<Type> plugin_type_list = new List<Type> ();
		List<IPlugin> active_plugin_list = new List<IPlugin> ();
		
		public PluginManager ()
		{
			Assembly a;
			Type [] types;
						
			string [] plugin_path = Global.Paths.PluginPath;
			foreach (string plugin_dir in plugin_path) {
				Global.Logger.Debug ("Plugin manager scanning {0}", plugin_dir);
				
				DirectoryInfo dir_info = new DirectoryInfo (plugin_dir);
				if (!dir_info.Exists)
					continue;
				
				FileInfo [] files = dir_info.GetFiles ();
				for (int i = 0; i < files.Length; i++) {
					if (files [i].Name.EndsWith (".dll")) {
						try {
							a = Assembly.LoadFile (files [i].FullName);
							types = a.GetTypes ();
							foreach (Type type in types) {
								if (type.GetInterface ("IPlugin") != null)
									plugin_type_list.Add (type);
							}
						} catch (Exception e) {
							Global.Logger.Warning ("Error loading plugins from assembly {0}: {1}", files [i].FullName, e.Message);
						}
					}
				}
			}
			
			plugin_type_list.Add (typeof(FakePassword));
			
			Global.Logger.Debug ("Plugin manager found {0} {1}", plugin_type_list.Count, plugin_type_list.Count == 1 ? "plugin" : "plugins");
			
			
			// Verify we have the required plugins
			if (IPassword == null) {
				Global.Logger.Error ("No plugin providing password storage found");
				throw new Exception ("No plugin providing password storage found");
			}
		}

		public IPlugin ActivatePlugin (Type plugin_type)
		{
			IPlugin plugin = (IPlugin) Activator.CreateInstance (plugin_type, new object [0]);
			plugin.Activate ();
			active_plugin_list.Add (plugin);
			
			return plugin;
		}
		
		public IPlugin GetProvider (string iface)
		{
			// Check the active list
			foreach (IPlugin plugin in active_plugin_list) {
				if (plugin.GetType ().GetInterface (iface) != null)
					return plugin;
			}
			
			// Check the inactive list
			foreach (Type plugin_type in plugin_type_list) {
				if (plugin_type.GetInterface (iface) != null)
					return ActivatePlugin (plugin_type);
			}
			
			return null;
		}
		
		public IPassword IPassword
		{
			get { return (IPassword) GetProvider ("IPassword"); }
		}
	}
}
