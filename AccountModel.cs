using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Chatter
{
	public delegate void AccountModelDelegate (AccountModel model);
	
	public class AccountModel
	{
		// Serializable stuff
		string protocol;
		string server;
		string account;
		bool require_encryption;
		uint port;
		bool auto_connect;
		string password;
		
		// Locals
		ConnectionModel conn;
		
		public AccountModel () { }
		
		public AccountModel (string protocol)
		{
			this.protocol = protocol;
			
			// Defaults
			server = "";
			account = "user@" + protocol;
			require_encryption = false;
			port = 0;
			auto_connect = false;
			password = "";
		}
		
		[XmlAttribute("Protocol")]
		public string Protocol
		{
			get { return protocol; }
			set { protocol = value; FireChanged (); }
		}
		
		[XmlElement("Server")]
		public string Server
		{
			get { return server; }
			set { server = value; FireChanged (); }
		}
		
		[XmlElement("Account")]
		public string AccountStr
		{
			get { return account; }
			set { account = value; FireChanged (); }
		}
		
		[XmlElement("RequireEncryption")]
		public bool RequireEncryption
		{
			get { return require_encryption; }
			set { require_encryption = value; FireChanged (); }
		}
		
		[XmlElement("AutoConnect")]
		public bool AutoConnect
		{
			get { return auto_connect; }
			set { auto_connect = value; FireChanged (); }
		}
		
		/*[XmlIgnore]
		public string Password
		{
			get
			{
				return Global.PluginManager.IPassword.GetPassword ("chatter-account-" + protocol + "-" + account);
			}
			set
			{
				Global.PluginManager.IPassword.SetPassword ("chatter-account-" + protocol + "-" + account, value);
			}
		}*/
		
		[XmlElement("Password")]
		public string Password
		{
			get
			{
				return password;
			}
			set
			{
				password = value; FireChanged();
			}
		}
		
		
		[XmlElement("Port")]
		public uint Port
		{
			get { return port; }
			set { port = value; FireChanged (); }
		}

		[XmlIgnore]
		public IDictionary<string, object> Parameters
		{
			get
			{
				IDictionary<string, object> list = new Dictionary<string, object> ();
				if (Server != "")
					list.Add (new KeyValuePair<string, object> ("server", Server));
				if (AccountStr != "")
					list.Add (new KeyValuePair<string, object> ("account", AccountStr));
				if (Password != "")
					list.Add (new KeyValuePair<string, object> ("password", Password));
				if (Port != 0)
					list.Add (new KeyValuePair<string, object> ("port", Port));
				
				list.Add (new KeyValuePair<string, object> ("ignore-ssl-errors", true));
				list.Add (new KeyValuePair<string, object> ("old-ssl", (bool) true));
				return list;
			}
		}
		
		void FireChanged ()
		{
			if (Changed != null) Changed (this);
		}

		public event AccountModelDelegate Changed;
	}
}
