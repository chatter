using System;
using System.Collections.Generic;
using Telepathy;
using org.freedesktop.DBus;
using NDesk.DBus;

namespace Chatter
{

	public delegate void ContactListDelegate ();
	
	/*
	Local cache of contact lists
	*/
	public class LocalContactList
	{
		ConnectionModel connection_model;
		IChannelContactList contact_list;
		
		List<ContactModel> members = new List<ContactModel> ();
		List<ContactModel> remote_pending = new List<ContactModel> ();
		List<ContactModel> local_pending = new List<ContactModel> ();
		
		public LocalContactList (ConnectionModel _connection_model, IChannelContactList _contact_list)
		{
			contact_list = _contact_list;
			connection_model = _connection_model;
			
			contact_list.MembersChanged += OnMembersChanged;
			
			// fixme: use GetAll ()
			uint [] member = contact_list.Members;
			foreach (uint handle in member)
				members.Add (connection_model.GetContactModelFromHandle (handle));
			
			uint [] local = contact_list.LocalPendingMembers;
			foreach (uint handle in local)
				local_pending.Add (connection_model.GetContactModelFromHandle (handle));
			
			uint [] remote = contact_list.RemotePendingMembers;
			foreach (uint handle in remote)
				remote_pending.Add (connection_model.GetContactModelFromHandle (handle));
		}
		
		void OnMembersChanged (string message, uint[] added, uint[] removed, uint[] local, uint[] remote, uint actor, uint reason)
		{
			foreach (uint handle in added)
				members.Add (connection_model.GetContactModelFromHandle (handle));
			
			foreach (uint handle in removed)
				members.Remove (connection_model.GetContactModelFromHandle (handle));
				
			local_pending.Clear ();
			foreach (uint handle in local)
				local_pending.Add (connection_model.GetContactModelFromHandle (handle));
				
			remote_pending.Clear ();
			foreach (uint handle in remote)
				remote_pending.Add (connection_model.GetContactModelFromHandle (handle));
			
			if (Changed != null) Changed ();
		}
		
		public List<ContactModel> Members
		{
			get { return members; }
		}
		
		public List<ContactModel> RemotePending
		{
			get { return remote_pending; }
		}
		
		public List<ContactModel> LocalPending
		{
			get { return local_pending; }
		}
		
		public event ContactListDelegate Changed;
	}
	
	
	public delegate void MultiplexStatusChangedDelegate (ConnectionModel conn, ConnectionStatus status, ConnectionStatusReason reason);
	
	/*
	A ConnectionModel is an active account
	
	ConnectionModel maintains copies of every list associated with the connection on this side
	of the dbus chatter. This can be thought of as a cache, and is only here to prevent querying
	dbus all the time.
	*/
	public class ConnectionModel
	{
		//static string [] well_known_list_names = new string [] {"subscribe", "publish", "hide", "allow", "deny"};
		static string [] well_known_list_names = new string [] {"subscribe"};
		
		AccountModel account;
		IConnection connection;
		string bus_name;
		
		Dictionary<uint, ContactModel> contact_model_dict = new Dictionary<uint, ContactModel> ();
		
		LocalContactList subscribe_list;
		LocalContactList publish_list;
		LocalContactList hide_list;
		LocalContactList allow_list;
		LocalContactList deny_list;
		
		public ConnectionModel (AccountModel _account)
		{
			account = _account;
			
			try
			{
				Global.Logger.Debug ("Request Connection {0}", account.AccountStr);
				
				IConnectionManager manager = ConnectionManager.FromProtocol (account.Protocol);
				ConnectionInfo info = manager.RequestConnection (account.Protocol, account.Parameters);
				
				bus_name = info.BusName;
			
				connection = Bus.Session.GetObject<IConnection> (bus_name, info.ObjectPath);
				
				connection.StatusChanged += OnStatusChanged;
				connection.Connect ();
			}
			catch (Exception e)
			{
				Console.WriteLine ("Failed to connect " + e);
				OnStatusChanged (ConnectionStatus.Disconnected, ConnectionStatusReason.NoneSpecified);
				return;
			}
		}

		public void Deactivate ()
		{
			connection.Disconnect ();
		}
		
		public IConnection Connection
		{
			get { return connection; }
		}
		
		public LocalContactList SubscribeList
		{
			get { return subscribe_list; }
		}
		
		public void OnStatusChanged (ConnectionStatus status, ConnectionStatusReason reason)
		{
			Global.Logger.Debug ("{0} Status change to {1}", account.AccountStr, status);
			switch (status)
			{
				case ConnectionStatus.Connected:
					InitConnected ();
					break;
				case ConnectionStatus.Connecting:
					break;
				case ConnectionStatus.Disconnected:
					break;
			}
			
			if (ConnectionStatusChanged != null) ConnectionStatusChanged (this, status, reason);
		}
		
		public void InitConnected ()
		{
			uint [] handles = connection.RequestHandles (HandleType.List, well_known_list_names);
			List<LocalContactList> objects = new List<LocalContactList> ();
			
			foreach (uint handle in handles)
			{
				ObjectPath path = connection.RequestChannel (ChannelType.ContactList, HandleType.List, handle, true);
				IChannelContactList icontact_list = Bus.Session.GetObject<IChannelContactList> (bus_name, path);
				objects.Add (new LocalContactList (this, icontact_list));
			}
			
			subscribe_list = objects [0];
//			publish_list = objects [1];
//			hide_list = objects [2];
//			allow_list = objects [3];
//			deny_list = objects [4];
		}
		
		public ContactModel GetContactModelFromHandle (uint handle)
		{
			if (!contact_model_dict.ContainsKey (handle))
			{
				ContactModel contact_model = new ContactModel (this, handle);
				contact_model_dict [handle] = contact_model;
				return contact_model;
			}
			
			return contact_model_dict [handle];
		}
		
		public AccountModel AccountModel
		{
			get { return account; }
		}
		
		public void InitiateTextChannel (ContactModel contact)
		{
			ObjectPath path = connection.RequestChannel (ChannelType.Text, HandleType.Contact, contact.Handle, true);
			IChannelText text_channel = Bus.Session.GetObject<IChannelText> (bus_name, path);
			
			contact.SetTextChannel (text_channel);
		}
		
		public event MultiplexStatusChangedDelegate ConnectionStatusChanged;
	}
	
}

