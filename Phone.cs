using System;
using System.Text;
using Gtk;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Chatter
{
	public class Phone
	{
		[DllImport("libc")]                                                     
		private static extern int prctl(int option, byte [] arg2, ulong arg3 , ulong arg4, ulong arg5);

		public static void SetProcessName (string name)
		{
			if (prctl (15 /* PR_SET_NAME */, Encoding.ASCII.GetBytes (name + '\0'), 0, 0, 
0) != 0)
				Global.Logger.Warning ("Error setting process name: " + Mono.Unix.Native.Stdlib.GetLastError());
		}
		
		static Widget.ChatterIcon icon;
		static Widget.ContactWindow contact_window;
		
		public static void Main ()
		{
			Gtk.Application.Init ();
			Gtk.Widget.DefaultColormap = Gdk.Screen.Default.RgbaColormap;
			SetProcessName ("Chatter");
			
			contact_window = new Widget.ContactWindow (Global.PersistientState.AccountStore, Global.ConnectionStore);
			contact_window.Show ();
			
			contact_window.TreeView.Activated += OnContactActivate;
			
			icon = new Widget.ChatterIcon ();
			icon.ButtonPress += delegate { contact_window.Visible = !contact_window.Visible; };
			icon.Show ();
			
			AccountModel [] accounts = Global.PersistientState.AccountStore.Accounts;
			foreach (AccountModel account in accounts)
			{
				if (account.AutoConnect)
					Global.ConnectionStore.Activate (account);
			}

			if (accounts.Length == 0)
			{
				Widget.LabelNotify label = new Widget.LabelNotify ("Welcome to Chatter\nTo add an account click on Edit -> Accounts");
				contact_window.AddNotify (label);
			}
			
			try
			{
				Application.Run ();
			}
			catch (Exception e)
			{
				Global.Logger.Error ("Fatal: " + e);
			}
			finally
			{
				// Clean up our connection mess if bad things happen
				// Note ctrl-c or seg fault will miss this
				Global.Cleanup ();
			}
		}
		
		public static void OnContactActivate (ContactModel contact)
		{
			Global.Logger.Debug ("Activate Contact {0}", contact.Name);
			
			// The made up policy is to activate a text channel
			contact.ConnectionModel.InitiateTextChannel (contact);
			contact.ConversationWindow.Show ();
		}
		
		/*		
		public static Widget.ChatterIcon NotificationIcon
		{
			get { return icon; }
		}
		*/
		public static void Quit ()
		{
			Application.Quit ();
		}
	}
}
