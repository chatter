using System;
using System.Collections.Generic;
using Telepathy;

namespace Chatter
{
	public delegate void ContactModelChangedDelegate ();

	/*
	Represents a contact
	
	Just a cache of stuff on the telepathy side really.
	*/
	public class ContactModel
	{
		uint handle;
		ConnectionModel connection_model;
		
		string name;
		string alias;
		IDictionary<string, IDictionary<string, object>> presence_items;
		
		Widget.ConversationWindow conversation_window;
		IChannelText text_channel;
		
		public ContactModel (ConnectionModel _connection_model, uint _handle)
		{
			handle = _handle;
			connection_model = _connection_model;
		}
		
		public uint Handle
		{
			get { return handle; }
		}
		
		public string Name
		{
			get
			{
				if (name == null) {
					string[] names = connection_model.Connection.InspectHandles (HandleType.Contact, new uint[] {handle});
					name = names[0];
				}
				return name;
			}
		}
		
		public ConnectionModel ConnectionModel
		{
			get { return connection_model; }
		}
		
		public Widget.ConversationWindow ConversationWindow
		{
			get
			{
				if (conversation_window == null)
				{
					conversation_window = new Widget.ConversationWindow (this);
				}
				return conversation_window;
			}
		}
		
		public void SetTextChannel (IChannelText _text_channel)
		{
			if (text_channel != null)
			{
				// fixme: dispose of old channel
			}
			
			text_channel = _text_channel;
			this.ConversationWindow.SetChannelText (text_channel);
		}
		
		
		
		/*
		public string Alias
		{
			get
			{
				if (alias == null) {
					//ALP FIXME alias = conn_model.Connection.RequestAlias (handle);
					FireUpdated ();
				}
				return alias;
			}
			set { alias = value; FireUpdated (); }
		}
		
		public IDictionary<string, IDictionary<string, object>> PresenceItems
		{
			get
			{
				if (presence_items == null) {
					presence_items = new Dictionary<string, IDictionary<string, object>> ();
					presence_items.Add (new KeyValuePair<string, IDictionary<string, object>> ("offline", null));
				}
				
				return presence_items;
			}
			set { presence_items = value; FireUpdated (); }
		}
				
		void FireUpdated ()
		{
			if (Updated != null) Updated (this, null);
		}
		*/
		public event ContactModelChangedDelegate Changed;
	}
}
