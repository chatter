using System;
using System.IO;

namespace Chatter
{
	public static class Global
	{
		static Widget.ImageCache cache;
		public static Widget.ImageCache ImageCache { get { return cache; } }
		
		static Paths paths;
		public static Paths Paths { get { return paths; } }
		
		static PersistientState persistient_state;
		public static PersistientState PersistientState { get { return persistient_state; } }
				
		static Logger logger;
		public static Logger Logger { get { return logger; } }
		
		static ConnectionStore connection_store;
		public static ConnectionStore ConnectionStore { get { return connection_store; } }
		
		static Plugin.PluginManager plugin_manager;
		public static Plugin.PluginManager PluginManager { get { return plugin_manager; } }
		
		static Global ()
		{
			// Warning: This order of creation is important
			paths = new Paths ();
			logger = new Logger ();
			cache = new Widget.ImageCache ();
			persistient_state = LoadState ();
			connection_store = new ConnectionStore ();
			plugin_manager = new Plugin.PluginManager ();
		}
		
		static PersistientState LoadState ()
		{			
			try {
				FileStream stream = new FileStream (Paths.ConfigFile, FileMode.Open, FileAccess.Read, FileShare.Read);				
				PersistientState state = PersistientState.FromStream (stream);
								
				Logger.Debug ("Loaded config file {0}", Paths.ConfigFile);
				return state;
				
			} catch (Exception e) {
				if (!File.Exists (Paths.ConfigFile)) {
					Logger.Message ("Config file at {0} does not exist, a new one will be written if changes are made", Paths.ConfigFile);
				} else {
					Logger.Message ("Failed to parse config file {0} because {1}, it will be overwritten", Paths.ConfigFile, e.Message);
				}
			}
			
			return PersistientState.FromNew ();
		}
		
		public static void Cleanup ()
		{
			Logger.Message ("Cleaning up");

			try {
				connection_store.Cleanup ();
			} catch (Exception e) {
				Global.Logger.Debug ("Exception thrown in cleanup {0}", e);
			}
			
			persistient_state.OutOfTime ();
		}
	}
	
	public class Paths
	{
		public string ConfigDir { get { return Path.Combine (HomeDir, ".chatter"); } }
		public string ConfigFile { get { return Path.Combine (ConfigDir, "chatter.xml"); } }
		
		public string [] PluginPath { get { return new string [] { Path.Combine (ConfigDir, "plugins") }; } }
		
		// This is from beagle/Util/PathFinder.cs
		string home;
		object home_lock = new object ();
		public string HomeDir {
			get {
				lock (home_lock) {
					if (home == null) {
						home = Environment.GetEnvironmentVariable ("CHATTER_HOME");
						if (home == null)
							home = Environment.GetEnvironmentVariable ("HOME");
						if (home == null)
							throw new Exception ("Couldn't get HOME or CHATTER_HOME");
						if (home.EndsWith ("/"))
							home = home.Remove (home.Length - 1, 1);
						home = Path.GetFullPath (home);
						if (! Directory.Exists (home))
							throw new Exception ("Home directory '" + home + "' doesn't exist");
					}
				}
				
				return home;
			}
		}
	}
}

