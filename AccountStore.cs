using System;
using System.Collections;
using System.Xml.Serialization;

namespace Chatter
{
	public class AccountStore
	{
		ArrayList account_list = new ArrayList ();
		
		public AccountStore () { }
		
		public int Count 
		{
			get { return account_list.Count; }
		}
						
		public void AddAccount (AccountModel account)
		{
			account_list.Add (account);
			if (AccountAdded != null) AccountAdded (account);
			FireCommitRequired (false);
			account.Changed += AccountChangedHandler;
		}
		
		public void RemoveAccount (AccountModel account)
		{			
			account.Changed -= AccountChangedHandler;
			account_list.Remove (account);
			if (AccountRemoved != null) AccountRemoved (account);
			FireCommitRequired (false);
		}
		
		public void LinkAccounts () {
			foreach (AccountModel account in account_list)
				account.Changed += AccountChangedHandler;
		}
		
		void AccountChangedHandler (AccountModel account) {
			if (AccountChanged != null) AccountChanged (account);
			FireCommitRequired (false);
		}
		
		[XmlElement ("Account")]
		public AccountModel [] Accounts
		{
			get
			{
				AccountModel [] accounts = new AccountModel [account_list.Count];
				account_list.CopyTo (accounts);
				return accounts;
			}
			set
			{
				foreach (AccountModel account in value)
					account_list.Add (account);
			}
		}
		
		public void FireCommitRequired (bool now)
		{
			if (CommitRequired != null) CommitRequired (this, now);
		}

		public event AccountModelDelegate AccountAdded;
		public event AccountModelDelegate AccountRemoved;
		public event AccountModelDelegate AccountChanged;
		
		public event StateCommitHandler CommitRequired;
	}
}
