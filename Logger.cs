using System;
using System.IO;
using System.Diagnostics;
using System.Reflection;

namespace Chatter
{
	public class Logger
	{
		public enum Level
		{
			Debug = 0,
			Message = 1,
			Warning = 2,
			Error = 3
		}
		
		Level min_print_level = Level.Debug;
		
		public Logger ()
		{
			Debug ("Logger started at {0}", DateTime.Now);
		}
		
		void Log (Level level, string mess, params object[] args)
		{
			if (level < min_print_level)
				return;

			StackTrace trace = new StackTrace ();
			StackFrame frame = trace.GetFrame (2);
			MethodBase method = frame.GetMethod ();
			
			Console.Write ("{0}.{1} (): {2}: ", method.DeclaringType, method.Name, level);
			Console.WriteLine (mess, args);
		}
		
		public void Debug (string mess, params object[] args)
		{
			Log (Level.Debug, mess, args);
		}
		
		public void Message (string mess, params object[] args)
		{
			Log (Level.Message, mess, args);
		}
		
		public void Warning (string mess, params object[] args)
		{
			Log (Level.Warning, mess, args);
		}
		
		public void Error (string mess, params object[] args)
		{
			Log (Level.Error, mess, args);
		}
	}
}
