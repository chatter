using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Telepathy;

namespace Chatter
{
	public delegate void ConnectionModelDelegate (AccountModel account, ConnectionModel connection);
	public delegate void StatusDelegate ();
	/*
	public delegate void ConnectionStoreDelegate (ConnectionModel connection);
	public delegate void ContactListDelegate (ContactListModel connection);
	public delegate void TextModelDelegate (TextModel text);
	public delegate void StreamedMediaModelDelegate (StreamedMediaModel text);
	*/
	
	/*
	A ConnectionStore is a store of ConnectionModels, which represent active accounts.
	
	An account being activated refers to the attempt to start a connection. A ConnectionModel
	exists iff an acount is active.
	
	An account being connected refers to being logged in and chatting with a remote
	entity.
	*/
	public class ConnectionStore
	{
		Dictionary<AccountModel, ConnectionModel> active_account_dict = new Dictionary<AccountModel, ConnectionModel> ();
		
		public bool IsActive (AccountModel account)
		{
			return active_account_dict.ContainsKey (account);
		}
		
		public ConnectionModel Activate (AccountModel account)
		{
			Debug.Assert (!IsActive (account));
			Global.Logger.Debug ("Activate account {0}", account.AccountStr);
			
			ConnectionModel model = new ConnectionModel (account);
			active_account_dict [account] = model;
			
			if (AccountActivate != null) AccountActivate (account, model);
			
			// One might think there is a race here, where a connection can be connected
			// before we hook up this signal. One must remember the synchronous nature of
			// D-Bus. The main loop will not run until this method returns.
			model.ConnectionStatusChanged += OnConnectionStatusChanged;
			
			return model;
		}
		
		// fixme: void Deactivate (ConnectionModel connection) ??
		public void Deactivate (AccountModel account)
		{
			Debug.Assert (IsActive (account));
			Global.Logger.Debug ("Deactivate account {0}", account.AccountStr);
			
			ConnectionModel model = active_account_dict [account];
			model.Deactivate ();
			
			active_account_dict.Remove (account);
			
			if (AccountDeactivate != null) AccountDeactivate (account, model);
		}
		
		public void Cleanup ()
		{
			while (active_account_dict.Keys.Count > 0)
			{
				foreach (AccountModel account in active_account_dict.Keys)
				{
					active_account_dict.Remove (account);
					break;
				}
			}
		}
		
		void OnConnectionStatusChanged (ConnectionModel model, ConnectionStatus status, ConnectionStatusReason reason)
		{
			switch (status)
			{
				case ConnectionStatus.Connected:
					if (AccountConnected != null) AccountConnected (model.AccountModel, model);
					break;
				case ConnectionStatus.Connecting:
					break;
				case ConnectionStatus.Disconnected:
					if (AccountDisconnected != null) AccountDisconnected (model.AccountModel, model);
					break;
			}
		}
		
		public event ConnectionModelDelegate AccountActivate;
		public event ConnectionModelDelegate AccountDeactivate;
		
		public event ConnectionModelDelegate AccountConnected;
		public event ConnectionModelDelegate AccountDisconnected;
	}
	
	public class Status
	{
		public void SetStatus ()
		{
			if (StatusChange != null) StatusChange ();
		}
		
		public void GetStatus ()
		{
		
		}
		
		public event StatusDelegate StatusChange;
		
		public IEnumerator GetContactEnumerator ()
		{
			return null;
		}
	}
}

