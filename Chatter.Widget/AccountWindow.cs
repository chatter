using System;
using System.IO;
using Gtk;
using Glade;
using Cairo;

namespace Chatter.Widget
{
	public class AccountWindow : Window
	{
		// Widgets
		[Widget] VBox account_top_vbox;
		
		[Widget] Alignment protocol_combo_container;
		ProtocolCombo protocol_combo = new ProtocolCombo ();
		
		[Widget] Label account_title;
		[Widget] ScrolledWindow account_view_container;
		AccountView account_view = new AccountView ();
		
		[Widget] HBox account_combo_container;
		AccountCombo account_combo;
		[Widget] Button remove_button;
				
		// Locals
		AccountStore account_store;
		AccountModel current;
				
		public AccountWindow (AccountStore _account_store) : base ("Your Accounts")
		{
			account_store = _account_store;
			
			InitUI ();
			
			ComboChangedHandler (null, null);
		}
		
		void InitUI ()
		{
			Glade.XML gxml = new Glade.XML (null, "phone.glade", "account_top_vbox", null);
			gxml.Autoconnect (this);
			
			account_view_container.Add (account_view);
			
			account_combo = new AccountCombo (account_store);
			account_combo_container.PackStart (account_combo, true, true, 0);
			
			Add (account_top_vbox);
			protocol_combo_container.Add (protocol_combo);
			
			DeleteEvent += delegate (object o, DeleteEventArgs args) { Visible = false; args.RetVal = true; };
			protocol_combo.Selected += ProtocolComboHandler;
			account_combo.Changed += ComboChangedHandler;
			remove_button.Clicked += RemoveClickedHandler;
			
			account_top_vbox.ShowAll ();
			Resize (300, 400);
		}
		
		new public bool Visible
		{
			get { return base.Visible; }
			set { base.Visible = value; Global.PersistientState.OutOfTime (); }
		}
		
		void ComboChangedHandler (object o, EventArgs args)
		{
			AccountModel account = account_combo.Selected;

			bool have_account = account != null;
			account_combo.Sensitive = have_account;
			account_view.Sensitive = have_account;
			remove_button.Sensitive = have_account;

			account_title.Markup = have_account ? "<b>" + account.AccountStr + "</b>" : "No existing accounts";

			current = account;
			account_view.Account = account;
		}
				
		void ProtocolComboHandler (string protocol)
		{
			AccountModel account = new AccountModel (protocol);
			account_store.AddAccount (account);
		}
		
		void RemoveClickedHandler (object o, EventArgs args)
		{
			account_store.RemoveAccount (account_combo.Selected);
		}
	}
}

