using System;
using System.Collections.Generic;
using Gtk;
using Glade;
using NDesk.DBus;

namespace Chatter.Widget
{
	public delegate void SelfPresenceChangedDelegate (IDictionary<string, IDictionary<string, object>> status);
	
	public class ContactWindow : Window
	{
		AccountWindow account_window;
		
		// Widgets
		[Widget] VBox contact_top_vbox;
		[Widget] VBox notify_container;
		
		[Widget] ScrolledWindow contact_tree_container;
		ContactTreeView contact_view;
		PresenceComboBox presence_combo = new PresenceComboBox ();
		
		[Widget] MenuItem menu_hide;
		[Widget] MenuItem menu_quit;
		[Widget] MenuItem menu_accounts;	

		ConnectionStore connection_store;
		
		Dictionary<AccountModel, AccountNotify> account_notify_dict = new Dictionary<AccountModel, AccountNotify> ();
		
		public ContactWindow (AccountStore _account_store, ConnectionStore _connection_store) : base ("Contacts")
		{
			contact_view = new ContactTreeView (_connection_store);

			connection_store = _connection_store;
			connection_store.AccountActivate += OnAccountActivate;
			connection_store.AccountDeactivate += OnAccountDeactivate;
			
			account_window = new AccountWindow (_account_store);
			
			InitUI ();
		}
		
		void InitUI ()
		{
			Glade.XML gxml = new Glade.XML (null, "phone.glade", "contact_top_vbox", null);
			gxml.Autoconnect (this);
			
			contact_tree_container.Add (contact_view);
			
			contact_top_vbox.PackStart (presence_combo, false, true, 0);
			Add (contact_top_vbox);
			
			DeleteEvent += delegate (object o, DeleteEventArgs args) { Visible = false; args.RetVal = true; };
			// presence_combo.Changed += PresenceChangedHandler;
			contact_top_vbox.ShowAll ();
			
			menu_hide.Activated += delegate (object o, EventArgs args) { Visible = false; };
			menu_quit.Activated += delegate (object o, EventArgs args) { Phone.Quit (); };
			menu_accounts.Activated += delegate (object o, EventArgs args) { account_window.Visible = !account_window.Visible; };
		}
				
		void OnAccountActivate (AccountModel account_model, ConnectionModel connection_model)
		{
			AccountNotify notify_widget = new AccountNotify (account_model, connection_model);
			account_notify_dict [account_model] = notify_widget;
			AddNotify (notify_widget);
		}

		void OnAccountDeactivate (AccountModel account_model, ConnectionModel connection_model)
		{
			if (account_notify_dict.ContainsKey (account_model))
			{
				RemoveNotify (account_notify_dict [account_model]);
			}
		}
		
		/*
		public AccountWindow AccountWindow
		{
			get { return account_window; }
			set { account_window = value; }
		}
		*/
		
		public void AddNotify (NotifyItem notify)
		{
			notify_container.Add (notify);
			notify.Show ();
			
			notify.Closed += RemoveNotify;
		}
		
		public void RemoveNotify (NotifyItem notify)
		{
			notify.Closed -= RemoveNotify;
			
			notify_container.Remove (notify);
			notify.Hide ();
		}
		
		public ContactTreeView TreeView
		{
			get { return contact_view; }
		}
	}
}

