using System;
using System.Collections.Generic;
using Gtk;
using NDesk.DBus;

namespace Chatter.Widget
{
	public class PresenceComboBox : ComboBox
	{
		public PresenceComboBox () : base (new string [] {"Available", "Be Right Back", "Busy", "Do Not Disturb", "Extended Away", "Hidden", "Offline"})
		{		
			InitUI ();
		}
		
		void InitUI ()
		{
			Active = 0;
		}

		public IDictionary<string, IDictionary<string, object>> Status
		{
			get
			{
				string str;
				switch (Active) {
					case 0:
						str = Telepathy.PresenceStr.Available;
						break;
					case 1:
						str = Telepathy.PresenceStr.Available; //Telepathy.PresenceStr.Brb;
						break;
					case 2:
						str = Telepathy.PresenceStr.Available; //Telepathy.PresenceStr.Busy;
						break;
					case 3:
						str = Telepathy.PresenceStr.Dnd;
						break;
					case 4:
						str = Telepathy.PresenceStr.ExtendedAway;
						break;
					case 5:
						str = Telepathy.PresenceStr.Available; //Telepathy.PresenceStr.Hidden;
						break;
					case 6:
						str = Telepathy.PresenceStr.Offline;
						break;
					default:
						throw new Exception ("Unknown status value");
				}
				Dictionary<string, IDictionary<string, object>> status = new Dictionary<string, IDictionary<string, object>> ();
				Dictionary<string, object> sub_status = new Dictionary<string, object> ();
				
				//ALPstatus.Add (new KeyValuePair<string, IDictionary<string, object>> (str, sub_status));
				status[str] = sub_status;
				return status;
			}
		}
	}
}
