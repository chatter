using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using Gtk;

namespace Chatter.Widget
{
	public class AccountView : TreeView
	{		
		AccountModel account;
		TreeStore store = new TreeStore (typeof (string));

		public AccountView () : base ()
		{		
			Model = store;
			
			TreeViewColumn column = new TreeViewColumn ();
			CellRendererText text_render = new CellRendererText ();
			text_render.Xalign = 0;
			column.PackStart (text_render, true);
			//column.SetCellDataFunc (text_render, CellLayoutText);
			column.SetCellDataFunc (text_render, (CellLayoutDataFunc)CellLayoutText);
			
			InsertColumn (column, 0);
			
			column = new TreeViewColumn ();
			CellRendererText text_render_edit = new CellRendererText ();
			text_render_edit.Editable = true;
			text_render_edit.Edited += EditedTextHandler;
			column.PackStart (text_render_edit, true);
			//ALP column.SetCellDataFunc (text_render_edit, CellLayoutTextEdit);
			column.SetCellDataFunc (text_render_edit, (CellLayoutDataFunc)CellLayoutTextEdit);
			
			CellRendererToggle toggle_render_edit = new CellRendererToggle ();
     		toggle_render_edit.Activatable = true;
    		toggle_render_edit.Toggled += ToggledHandler;
			column.PackStart (toggle_render_edit, true);
			//ALP column.SetCellDataFunc (toggle_render_edit, CellLayoutToggleEdit);
			column.SetCellDataFunc (toggle_render_edit, (CellLayoutDataFunc)CellLayoutToggleEdit);
			
			InsertColumn (column, 1);
			
			HeadersVisible = false;
		}
		
		public AccountModel Account
		{
			get { return account; }
			set
			{			
				account = value;
					
				TreeIter parent = TreeIter.Zero, child;
				if (account != null) {
					parent = store.AppendNode ();
					store.SetValue (parent, 0, "Status");
					child = store.AppendNode (parent);
					store.SetValue (child, 0, "Protocol");
					child = store.AppendNode (parent);
					store.SetValue (child, 0, "Connected");
					parent = store.AppendNode ();
					store.SetValue (parent, 0, "Required settings");
					child = store.AppendNode (parent);
					store.SetValue (child, 0, "Account");
					child = store.AppendNode (parent);
					store.SetValue (child, 0, "Password");
					parent = store.AppendNode ();
					store.SetValue (parent, 0, "General settings");
					child = store.AppendNode (parent);
					store.SetValue (child, 0, "Auto connect");
					parent = store.AppendNode ();
					store.SetValue (parent, 0, "Advanced");
					child = store.AppendNode (parent);
					store.SetValue (child, 0, "Server");
					child = store.AppendNode (parent);
					store.SetValue (child, 0, "Port");

					ExpandAll ();
				}
			}
		}
				
		void CellLayoutText (CellLayout cell_layout, CellRenderer cell_render, TreeModel model, TreeIter it)
		{
			CellRendererText text_render = (CellRendererText) cell_render;
			text_render.Text = (string) model.GetValue (it, 0);
		}
		
		void CellLayoutTextEdit (CellLayout cell_layout, CellRenderer cell_render, TreeModel model, TreeIter it)
		{
			CellRendererText text_render = (CellRendererText) cell_render;
			string name = (string) model.GetValue (it, 0);
			
			text_render.Visible = true;
			text_render.Editable = true;
			
			switch (name) {
				case "Protocol":
					text_render.Editable = false;
					text_render.Text = account.Protocol;
					break;
				case "Account":
					text_render.Text = account.AccountStr;
					break;
				case "Password":
					text_render.Text = new string ('*', account.Password.Length);
					break;
				case "Server":
					text_render.Text = account.Server;
					break;
				case "Port":
					if (account.Port == 0)
						text_render.Text = "";
					else
						text_render.Text = account.Port.ToString ();
					break;
				default:
					text_render.Visible = false;
					break;
			}
		}
		
		void EditedTextHandler (object o, EditedArgs args)
		{
			TreeIter it;
			if (!store.GetIter (out it, new TreePath (args.Path)))
				return;
			string name = (string) store.GetValue (it, 0);
			
			switch (name) {
				case "Account":
					account.AccountStr = args.NewText;
					break;
				case "Password":
					account.Password = args.NewText;
					break;
				case "Server":
					account.Server = args.NewText;
					break;
				case "Port":
					account.Port = UInt32.Parse (args.NewText);
					break;
			}
		}
		
		void CellLayoutToggleEdit (CellLayout cell_layout, CellRenderer cell_render, TreeModel model, TreeIter it)
		{
			CellRendererToggle toggle_render = (CellRendererToggle) cell_render;
			string name = (string) model.GetValue (it, 0);
			
			toggle_render.Visible = true;
			
			switch (name) {
				case "Connected":
					toggle_render.Active = Global.ConnectionStore.IsActive (account);
					break;
				case "Auto connect":
					toggle_render.Active = account.AutoConnect;
					break;
				default:
					toggle_render.Visible = false;
					break;
			}
		}
		
		void ToggledHandler (object o, ToggledArgs args)
		{
			TreeIter it;
			if (!store.GetIter (out it, new TreePath (args.Path)))
				return;
			string name = (string) store.GetValue (it, 0);
			
			switch (name) {
				case "Connected":
					if (Global.ConnectionStore.IsActive (account))
						Global.ConnectionStore.Deactivate (account);
					else
						Global.ConnectionStore.Activate (account);
					break;
				case "Auto connect":
					account.AutoConnect = !account.AutoConnect;
					break;
			}
		}
	}
}

