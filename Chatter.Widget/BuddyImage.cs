using System;
using System.IO;
using Gtk;
using Cairo;

namespace Chatter.Widget
{
	public class CairoDrawingArea : DrawingArea
	{
		public static void RectangleCurve (Context g, double x0, double y0, double rect_width, double rect_height, double radius)
        {
        		double x1 = x0 + rect_width;
			double y1 = y0 + rect_height;
			
			if (rect_width <= 0 || rect_height <= 0)
			    return;
			
			if (rect_width / 2 < radius) {
				if (rect_height / 2 < radius) {
					g.MoveTo (x0, (y0 + y1)/2);
			        g.CurveTo (x0 ,y0, x0, y0, (x0 + x1)/2, y0);
			        g.CurveTo (x1, y0, x1, y0, x1, (y0 + y1)/2);
			        g.CurveTo (x1, y1, x1, y1, (x1 + x0)/2, y1);
			        g.CurveTo (x0, y1, x0, y1, x0, (y0 + y1)/2);
			    } else {
			        g.MoveTo (x0, y0 + radius);
			        g.CurveTo (x0 ,y0, x0, y0, (x0 + x1)/2, y0);
			        g.CurveTo (x1, y0, x1, y0, x1, y0 + radius);
			        g.LineTo (x1 , y1 - radius);
			        g.CurveTo (x1, y1, x1, y1, (x1 + x0)/2, y1);
			        g.CurveTo (x0, y1, x0, y1, x0, y1- radius);
			    }
			} else {
			    if (rect_height/2<radius) {
			        g.MoveTo (x0, (y0 + y1)/2);
			        g.CurveTo (x0 , y0, x0 , y0, x0 + radius, y0);
			        g.LineTo (x1 - radius, y0);
			        g.CurveTo (x1, y0, x1, y0, x1, (y0 + y1)/2);
			        g.CurveTo (x1, y1, x1, y1, x1 - radius, y1);
			        g.LineTo (x0 + radius, y1);
			        g.CurveTo (x0, y1, x0, y1, x0, (y0 + y1)/2);
			    } else {
			        g.MoveTo (x0, y0 + radius);
			        g.CurveTo (x0 , y0, x0 , y0, x0 + radius, y0);
			        g.LineTo (x1 - radius, y0);
			        g.CurveTo (x1, y0, x1, y0, x1, y0 + radius);
			        g.LineTo (x1 , y1 - radius);
			        g.CurveTo (x1, y1, x1, y1, x1 - radius, y1);
			        g.LineTo (x0 + radius, y1);
			        g.CurveTo (x0, y1, x0, y1, x0, y1- radius);
			    }
			}
			
			g.ClosePath ();
        }
        
		protected override bool OnExposeEvent (Gdk.EventExpose args)
		{
			Gdk.Window win = args.Window;
			       
			using (Cairo.Context g = Gdk.Context.CreateDrawable (win, true))
			{
				Draw (g);
			}

			return true;
        }
        
		protected virtual void Draw (Cairo.Context g) { }
	}
	
	public class BuddyImage : CairoDrawingArea
	{
		public const int AnimationLength = 2000;
		
		ImageSurface buddy_surface_old;
		ImageSurface buddy_surface_current;
		int tick_count_start;
		
		public BuddyImage ()
		{
			SetSizeRequest (40, 40);
		}
				
		public ImageSurface Image
		{
			get { return buddy_surface_current; }
			set
			{
				if (buddy_surface_current != value)
				{
					Show ();
					buddy_surface_old = buddy_surface_current;
					buddy_surface_current = value;

					tick_count_start = System.Environment.TickCount;
					GLib.Timeout.Add (1000 / 20, OnTimeout);
				}
			}
		}
		
		protected bool OnTimeout ()
		{
			if (System.Environment.TickCount - tick_count_start > AnimationLength) {
				buddy_surface_old = null;
				if (buddy_surface_current == null)
					Hide ();
				return false;
			}

			QueueDraw ();
			return true;
		}
		
		protected void DrawSurface (Cairo.Context g, ImageSurface s, double alpha)
		{
			if (s == null) {
				g.Color = new Color (1, 1, 1);
				g.PaintWithAlpha (alpha);
				return;
			}
			
			Pattern pattern = new Pattern (s);
			Matrix matrix = new Matrix ();
			matrix.InitScale ((double)s.Width / (Allocation.Width - 4), (double)s.Height / (Allocation.Height - 4));
			matrix.Translate (-2, -2);
			pattern.Matrix = matrix;
			g.Pattern = pattern;

			g.PaintWithAlpha (alpha);
		}
		
		protected override void Draw (Cairo.Context g)
		{
			if (System.Environment.TickCount - tick_count_start < AnimationLength) {
				DrawSurface (g, buddy_surface_old, 1);
			
				double blend = (System.Environment.TickCount - tick_count_start) / (double) AnimationLength;
				DrawSurface (g, buddy_surface_current, blend);
			} else {
				DrawSurface (g, buddy_surface_current, 1);
			}
			
			g.LineWidth = 1;
			
			g.Rectangle (0.5, 0.5, Allocation.Width - 1, Allocation.Height - 1);
			g.Color = new Color (0, 0, 0);
			g.Stroke ();
			
			g.Rectangle (1.5, 1.5, Allocation.Width - 3, Allocation.Height - 3);
			g.Color = new Color (1, 1, 1);
			g.Stroke ();
		}
	}
}
