using System;
using System.Collections.Generic;
using System.IO;
using Gtk;
using Glade;
using Cairo;
using Telepathy;

namespace Chatter.Widget
{
	public class TextComponent : VBox
	{
		ContactModel contact_model;
		IChannelText channel_text;
	
		ScrolledWindow text_view_container = new ScrolledWindow ();
		
		TextView text_view = new TextView ();
		TextBuffer text_buffer;
		
		HBox text_entry_container = new HBox ();
		ChatEntry text_input = new ChatEntry ();
		SmileButton smiley_button = new SmileButton ();
		
		uint last = 0xffff;
		
		TextTag tag_recv_strong = new TextTag ("recv_strong");
		TextTag tag_recv_weak = new TextTag ("recv_weak");
		TextTag tag_sent_strong = new TextTag ("sent_strong");
		TextTag tag_sent_weak = new TextTag ("sent_weak");

		public TextComponent (ContactModel _contact_model, IChannelText _channel_text)
		{
			contact_model = _contact_model;
			channel_text = _channel_text;
			
			channel_text.Sent += TextSentHandler;
			channel_text.Received += TextReceivedHandler;
					
			InitUI ();
			text_input.Activated += InputActivatedHandler;
			
			//disable non-working things
			smiley_button.Visible = false;
		}
		
		void InitUI ()
		{
			text_buffer = text_view.Buffer;
			TextTagTable tag_table = text_buffer.TagTable;
			
			tag_recv_strong.ForegroundGdk = new Gdk.Color (10, 100, 10);
			tag_recv_strong.Weight = Pango.Weight.Bold;
			tag_table.Add (tag_recv_strong);
			tag_recv_weak.ForegroundGdk = new Gdk.Color (0, 30, 0);
			tag_table.Add (tag_recv_weak);
			tag_sent_strong.ForegroundGdk = new Gdk.Color (100, 10, 10);
			tag_sent_strong.Weight = Pango.Weight.Bold;
			tag_table.Add (tag_sent_strong);
			tag_sent_weak.ForegroundGdk = new Gdk.Color (30, 0, 0);
			tag_table.Add (tag_sent_weak);
			
			text_view.Editable = false;
			text_view.CursorVisible = false;
			text_view.WrapMode = WrapMode.Word;
			
			text_view_container.Add (text_view);
			PackStart (text_view_container, true, true, 0);
			
			text_entry_container.PackStart (text_input, true, true, 0);
			text_entry_container.PackStart (smiley_button, false, true, 0);
			text_entry_container.Spacing = 5;
			PackStart (text_entry_container, false, true, 0);

			Spacing = 3;
		}
		
		void TextReceivedHandler (uint id, uint timestamp, uint sender, Telepathy.MessageType type, Telepathy.MessageFlag flags, string text)
		{
			//FIXME ALP text_model.Text.AcknowledgePendingMessage (id);
			AddTextRecv (contact_model, text);
			text_view_container.Vadjustment.Value = text_view_container.Vadjustment.Upper;
		}
		
		void TextSentHandler (uint timestamp, Telepathy.MessageType type, string text)
		{
			AddTextSent (text);
			text_view_container.Vadjustment.Value = text_view_container.Vadjustment.Upper;
		}
		
		void InputActivatedHandler (object o, EventArgs args)
		{
			if (text_input.Text != "") {
	//			if (text_model == null)
	//				Model = contact_model.RequestTextModel ();
				
				channel_text.Send (Telepathy.MessageType.Normal, text_input.Text);
				text_input.Text = "";
			}
		}
		
		void AddTextRecv (ContactModel contact, string text)
		{
			TextIter it = text_buffer.EndIter;
			if (last != contact.Handle) {
				last = contact.Handle;
				text_buffer.InsertWithTags (ref it, contact.Name + ":\n", tag_recv_strong);
			}
			
			text_buffer.InsertWithTags (ref it, "  " + text + "\n", tag_recv_weak);
		}
		
		void AddTextSent (string text)
		{
			TextIter it = text_buffer.EndIter;
			if (last != 0) {
				last = 0;
				text_buffer.InsertWithTags (ref it, "Me:\n", tag_sent_strong);
			}
			
			text_buffer.InsertWithTags (ref it, "  " + text + "\n", tag_sent_weak);
		}
	}
	
	public class SmileButton : Gtk.Button
	{			
		public SmileButton ()
		{
			Add (Global.ImageCache.GetImage ("SmileButton_smile.png"));
		}
	}
	
	public class ChatEntry : Entry
	{
		public ChatEntry ()
		{
			
		}
	}
}
