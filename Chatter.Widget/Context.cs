using System;
using System.Reflection;
using System.Runtime.InteropServices;
using Cairo;
using Gtk;

namespace Gdk
{
        public class Context
        {
                //Use [DllImport("libgdk-win32-2.0-0.dll")] for  Win32 
                [DllImport("libgdk-x11-2.0.so")]
                internal static extern IntPtr gdk_x11_drawable_get_xdisplay (IntPtr raw);

                [DllImport("libgdk-x11-2.0.so")]
                  internal static extern IntPtr gdk_x11_drawable_get_xid (IntPtr raw);

                [DllImport("libgdk-x11-2.0.so")]
                  internal static extern IntPtr gdk_drawable_get_visual (IntPtr raw);

                [DllImport("libgdk-x11-2.0.so")]
                  internal static extern IntPtr gdk_x11_visual_get_xvisual (IntPtr raw);

                public static Cairo.Context CreateDrawable (Gdk.Drawable drawable, bool add_offset)
                {
                        IntPtr x_drawable = IntPtr.Zero;
                        int x_off = 0, y_off = 0;

                        int x, y, w, h, d;
                        ((Gdk.Window)drawable).GetGeometry(out x, out y, out w, out h, out d);

                        if (add_offset)
                          ((Gdk.Window) drawable).GetInternalPaintInfo(out drawable,
                                                                    out x_off, out y_off);

                        x_drawable = drawable.Handle;
                        IntPtr visual = gdk_drawable_get_visual(x_drawable);

                        IntPtr Xdisplay = gdk_x11_drawable_get_xdisplay(x_drawable);
                        IntPtr Xvisual = gdk_x11_visual_get_xvisual(visual);
                        IntPtr Xdrawable = gdk_x11_drawable_get_xid (x_drawable);

                        Cairo.XlibSurface s = new Cairo.XlibSurface (Xdisplay,
                                                                   Xdrawable,
                                                                   Xvisual,
                                                                   w, h);

                        Cairo.Context g = new Cairo.Context (s);

                        if (add_offset)
                          g.Translate (-(double)x_off,-(double)y_off);

                        return g;
                }
        }               
}   
