using System;
using System.Collections;
using Gtk;

namespace Chatter.Widget
{
	// Hack of the year!
	public class HackedComboBox : ComboBox
	{		
		CellView child_cell;
		TreePath child_first_path;
		
		public HackedComboBox (Gdk.Pixbuf title_pixbuf, string title) : base (new ListStore (typeof (Gdk.Pixbuf), typeof (string)))
		{			
			CellRendererPixbuf cell_rend_pixbuf = new CellRendererPixbuf ();
			CellRendererText cell_rend_text = new CellRendererText ();
			PackStart (cell_rend_pixbuf, false);
			PackStart (cell_rend_text, true);
			
			SetAttributes (cell_rend_pixbuf, "pixbuf", 0);
			SetAttributes (cell_rend_text, "text", 1);
			
			ListStore child_model = new ListStore (typeof (Gdk.Pixbuf), typeof (string));
			child_cell = (Child as CellView);
			child_cell.Model = child_model;
			
			TreeIter it = child_model.AppendValues (title_pixbuf, title);
			child_first_path = child_model.GetPath (it);
			
			FocusOnClick = false;
			CanFocus = false;
			Reset ();
		}
		
		void Reset ()
		{
			Active = -1;
			child_cell.DisplayedRow = child_first_path;
		}
		
		virtual protected void OnOption (int selection) { }
		
		protected override void OnChanged ()
		{
			if (Active < 0)
				return;
			
			OnOption (Active);
						
			Reset ();
		}
		
		protected new ListStore Model
		{
			get { return (ListStore) base.Model; }
		}
	}
	
	public class ActionCombo : HackedComboBox
	{				
		public ActionCombo () : base (Global.ImageCache.GetPixbuf ("ActionCombo_Action.png"), "Action")
		{
			Model.AppendValues (null, "Send file");
			Model.AppendValues (null, "Nudge");
		}

		protected override void OnOption (int selection)
		{			
			switch (selection) {
				case 0:
					if (SendActivated != null) SendActivated (this, null);
					break;
				case 1:
					if (NudgeActivated != null) NudgeActivated (this, null);
					break;
			}
		}
		
		public event EventHandler SendActivated;
		public event EventHandler NudgeActivated;
	}
	
	public delegate void ProtocolComboSelectedDelegate (string protocol);
	
	public class ProtocolCombo : HackedComboBox
	{				
		public ProtocolCombo () : base (Global.ImageCache.GetPixbuf ("ActionCombo_Action.png"), "New Account")
		{
			Model.AppendValues (null, "Jabber");
			Model.AppendValues (null, "Google Talk");
			Model.AppendValues (null, "Msn");
		}

		protected override void OnOption (int selection)
		{
			string protocol = null;
			
			switch (selection) {
				case 0:
					protocol = "jabber";
					break;
				case 1:
					protocol = "google-talk";
					break;
				case 2:
					protocol = "msn";
					break;
				default:
					Global.Logger.Error ("Unknown account protocol");
					return;
			}
			
			if (Selected != null) Selected (protocol);
		}
		
		public event ProtocolComboSelectedDelegate Selected;
	}
}

