using System;
using System.Collections.Generic;
using Gtk;


namespace Chatter.Widget
{
	public delegate void ContactStoreDelegate ();
	/*
	Responsible for maintaining a list to show to the user, a conglomeration of lists
	from every account.
	*/
	public class ContactStore : ListStore
	{
		ConnectionStore connection_store;
		
		public ContactStore (ConnectionStore _connection_store) : base (typeof(object))
		{
			connection_store = _connection_store;
			
			connection_store.AccountConnected += OnAccountConnected;
		}
		
		void OnAccountConnected (AccountModel account, ConnectionModel connection)
		{
			foreach (ContactModel contact in connection.SubscribeList.Members)
				this.AppendValues(contact);
		}
		
		public event ContactStoreDelegate Changed;
	}
	
	public delegate void ContactActivatedDelegate (ContactModel model);
	
	public class ContactTreeView : TreeView
	{
		ContactStore store;
		TreeModelFilter filter;
		
		public ContactTreeView (ConnectionStore _connection_store)
		{
			store = new ContactStore (_connection_store);
			store.Changed += delegate { QueueDraw (); };
			
			InitUI ();
		}
		
		void InitUI ()
		{
			CellRendererText text_render = new CellRendererText ();
			CellRendererPixbuf pixbuf_render = new CellRendererPixbuf ();
			TreeViewColumn column;
			
			column = new TreeViewColumn ();
			column.PackStart (pixbuf_render, false);
			column.PackStart (text_render, true);
			column.SetCellDataFunc (pixbuf_render, (CellLayoutDataFunc)CellLayoutPixbuf);
			column.SetCellDataFunc (text_render, (CellLayoutDataFunc)CellLayoutText);
			InsertColumn (column, 0);
			
			filter = new TreeModelFilter (store, null);
			filter.VisibleFunc = FilterFunc;
			
			Model = filter;
			
			HeadersVisible = false;
		}
		
		void CellLayoutText (CellLayout cell_layout, CellRenderer cell_render, TreeModel model, TreeIter it)
		{
			CellRendererText text_render = (CellRendererText) cell_render;
			ContactModel contact = (ContactModel) model.GetValue (it, 0);
			
			text_render.Text = contact.Name;
		}
		
		void CellLayoutPixbuf (CellLayout cell_layout, CellRenderer cell_render, TreeModel model, TreeIter it)
		{
			CellRendererPixbuf pixbuf_render = (CellRendererPixbuf) cell_render;
			ContactModel contact = (ContactModel) model.GetValue (it, 0);
			
			//FIXME ALP pixbuf_render.Pixbuf = Global.ImageCache.GetPixbufPresence (contact.PresenceItems [0].Key, ImageSize.Big);
		}
		
		bool FilterFunc (TreeModel model, TreeIter it)
		{
			return true;
			// Disable filtering for now. It was confusing me
			/*ContactModel contact = (ContactModel) model.GetValue (it, 0);
			// WTF: How can contact be null here?
			// Does it filter before we can set the value?
			if (contact == null)
				return true;
			
			string presence;
			if (contact.PresenceItems == null) {
				presence = "unknown";
			} else {
				if (contact.PresenceItems.Count == 0)
					presence = "unknown";
				else
					presence = contact.PresenceItems [0].Key;
			}
			switch (presence) {
				case "unknown":
//				case "offline":
					return false;
				default:
					return true;
			}*/
		}
		
		override protected void OnRowActivated (TreePath path, TreeViewColumn col)
		{
			TreeIter it;
			filter.GetIter (out it, path);
			ContactModel contact = (ContactModel) filter.GetValue (it, 0);
			
			if (Activated != null) Activated (contact);
			//FIXME ALP if (contact.PresenceItems [0].Key != "offline")
			//	Phone.NotificationIcon.ActivateContactText (contact);
		}
		
		public void UpdateRequest (object o, EventArgs args)
		{
			QueueDraw ();
		}
		
		public event ContactActivatedDelegate Activated;
	}
}

