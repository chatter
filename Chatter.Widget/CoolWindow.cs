using System;
using System.IO;
using Gtk;
using Glade;
using Cairo;

namespace Chatter.Widget
{
	public class CoolWindow : Window
	{
		public CoolWindow (string name) : base (name)
		{
			Decorated = false;
			BorderWidth = 8;

			ButtonPressEvent += ButtonPressHandler;
			Events = Gdk.EventMask.ButtonPressMask | Gdk.EventMask.ButtonReleaseMask;
		}
				
		protected override bool OnExposeEvent (Gdk.EventExpose args)
		{
			Gdk.Window win = args.Window;
			int x, y, w, h, d;
			win.GetGeometry(out x, out y, out w, out h, out d);
			       
			using (Cairo.Context g = Gdk.Context.CreateDrawable (win, true))
			{
				Draw (g);
			}
			
			foreach (Gtk.Widget child in Children)
				PropagateExpose (child, args);
				
			return false;
        }
				
        void ButtonPressHandler (object o, ButtonPressEventArgs args)
        {
        		double clickx = args.Event.X;
        		double clicky = args.Event.Y;
				int window_width, window_height;
				
        		GetSize (out window_width, out window_height);
        		
        		if ((clickx > window_width - 35) && (clicky > window_height - 35)) {
        			BeginResizeDrag (Gdk.WindowEdge.SouthEast, (int) args.Event.Button, (int) args.Event.XRoot, (int) args.Event.YRoot, args.Event.Time);
        		} else {
        			BeginMoveDrag ((int) args.Event.Button, (int) args.Event.XRoot, (int) args.Event.YRoot, args.Event.Time);
        		}
		}

        protected virtual void Draw (Cairo.Context g)
        {
        	double width = Allocation.Width;
        	double height = Allocation.Height;
        		
        	g.Operator = Operator.Source;
        	g.Color = new Color (0, 0, 0, 0);
        	g.Paint ();
        		
        	g.Operator = Operator.Over;

        	CairoDrawingArea.RectangleCurve (g, 3, 3, width - 6, height - 6, 30);
        	g.Clip ();
        		        		
       	 	LinearGradient pattern = new LinearGradient (0.0, 0.0, 0.0, height);
       	 	pattern.AddColorStop (0, new Color (0.8, 0.8, 1, 1));
			pattern.AddColorStop (0.5, new Color (0.8, 0.8, 1, 0.8));
			pattern.AddColorStop (1, new Color (0.8, 0.8, 1, 1));
			g.Pattern = pattern;
			g.Paint ();
			pattern.Destroy ();
			
			CairoDrawingArea.RectangleCurve (g, 3, 3, width - 6, height - 6, 30);
			g.LineWidth = 2;
        		g.Color = new Color (0.6, 0.6, 1, 1);
        		g.Stroke ();
        		
			g.Color = new Color (0.3, 0.3, 0.3, 0.8);
			g.LineWidth = 0.6;
			for (double offset = 30; offset > 13; offset -= 3)
			{
				g.MoveTo (width - offset, height - 7);
				g.LineTo (width - 7, height - offset);
				g.Stroke ();
			}
        }
	}
}
