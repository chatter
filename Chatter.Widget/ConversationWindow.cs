using System;
using System.IO;
using Gtk;
using Glade;
using Cairo;
using Telepathy;

namespace Chatter.Widget
{
	public class ConversationWindow : CoolWindow
	{
		VBox conv_top_vbox = new VBox ();
		HBox presence_container = new HBox ();
		VBox left_vbox = new VBox ();
		HBox title_hbox = new HBox ();
		Image presence_image = new Image ();
		Label title_label = new Label ();
		
		HBox button_row_container = new HBox ();
		Button text_button = new Button ("Text");
		DialButton audio_button = new DialButton ();
		Button video_button = new Button ("Video");
		AudioComponent audio_component;
		ActionCombo action_combo = new ActionCombo ();
		
		AspectFrame buddy_image_frame = new AspectFrame (null, 1f, 0f, 1f, false);
		BuddyImage buddy_image = new BuddyImage ();
		
		Alignment close_button_container = new Alignment (1f, 0f, 0f, 0f);
		Button close_button = new Button ();
		
		VideoComponent video_component;
		TextComponent text_component;
		
		ContactModel contact_model;
				
		public ConversationWindow (ContactModel _contact_model) : base ("")
		{
			InitUI ();
			
			contact_model = _contact_model;

			contact_model.Changed += OnContactChanged;
			OnContactChanged ();
			
			//Disable non-working things
			action_combo.Visible = false;
			audio_button.Visible = false;
			video_button.Visible = false;
			text_button.Visible = false;
		}
		
		public void SetChannelText (IChannelText channel)
		{
			text_component = new TextComponent (contact_model, channel);
			text_component.ShowAll ();
			conv_top_vbox.PackStart (text_component, true, true, 0);
		}
		
		public ImageSurface Image
		{
			get { return buddy_image.Image; }
			set { buddy_image.Image = value; }
		}
			
		void InitUI ()
		{
			title_hbox.PackStart (presence_image, false, true, 0);
			title_label.Justify = Justification.Left;
			title_label.Ellipsize = Pango.EllipsizeMode.End;
			title_hbox.PackStart (title_label, true, true, 0);
			title_hbox.Spacing = 2;
			left_vbox.PackStart (title_hbox, false, true, 0);
			
			button_row_container.PackStart (text_button, false, false, 0);
			button_row_container.PackStart (audio_button, false, false, 0);
		//	button_row_container.PackStart (audio_component, false, false, 0);
			button_row_container.PackStart (video_button, false, false, 0);
			button_row_container.PackStart (action_combo, false, false, 0);
			button_row_container.Spacing = 2;
			left_vbox.PackStart (button_row_container, false, true, 0);
			
			left_vbox.Spacing = 3;
			presence_container.Add (left_vbox);
			
			buddy_image_frame.Add (buddy_image);
			presence_container.PackStart (buddy_image_frame, false, true, 0);
			
			close_button.Image = new Image (Stock.Close, IconSize.Menu);
			close_button_container.Add (close_button);
			presence_container.PackStart (close_button_container, false, false, 0);
			
			presence_container.BorderWidth = 2;
			conv_top_vbox.PackStart (presence_container, false, true, 0);
		//	conv_top_vbox.PackStart (video_component, false, true, 0);
			
			
			conv_top_vbox.Spacing = 2;
			conv_top_vbox.BorderWidth = 1;
			Add (conv_top_vbox);
			conv_top_vbox.ShowAll ();
			
			buddy_image_frame.Visible = false;
			
			text_button.Clicked += delegate (object o, EventArgs args) { text_component.Visible = !text_component.Visible; };
			audio_button.Clicked += delegate (object o, EventArgs args) { audio_component.Visible = !audio_component.Visible; };
			DeleteEvent += delegate (object o, DeleteEventArgs args) { CloseHandler (o, args); args.RetVal = true; };
			close_button.Clicked += CloseHandler;
		}
		
		void CloseHandler (object o, EventArgs args)
		{
			Hide ();
						
			//Phone.NotificationIcon.ForgetConversationWindow (this);
		}
		
		void OnContactChanged ()
		{
			Title = contact_model.Name;
			title_label.Markup = "<span font_desc='12'><b>" + contact_model.Name + "</b></span>\n<span font_desc='8' color='#505050'>" + "</span>";
			//FIXME ALP presence_image.Pixbuf = Global.ImageCache.GetPixbufPresence (contact_model.PresenceItems [0].Key, ImageSize.Big);
		}		
	}
}
