using System;
using System.Collections.Generic;
using Gtk;

namespace Chatter.Widget
{
	public class AudioComponent : HBox
	{
	/*
		ContactModel contact_model;
		StreamedMediaModel media_model;
		
		SoundButton sound_button = new SoundButton ();
		
		public AudioComponent ()
		{
			PackStart (sound_button, false, false, 0);
			Spacing = 2;
		}
		
		public void ForgetModel ()
		{
			if (media_model != null) {
				StreamedMediaModel model = media_model;
				Model = null;
				model.Dispose ();
			}
		}
		
		public StreamedMediaModel Model
		{
			get { return media_model; }
			set
			{
				if (media_model != null) {
					if (value != null) {
						uint my_handle, val_handle;
						Telepathy.HandleType my_type, val_type;
						//ALP FIXME media_model.StreamedMedia.GetHandle (out my_type, out my_handle);
						//ALP FIXME value.StreamedMedia.GetHandle (out val_type, out val_handle);
						//dummy 0 by ALP
						my_handle = 0;
						val_handle = 0;
						if (my_handle == val_handle)
							return;
					}
					
					//ALP FIXME media_model.StreamedMedia.NewMediaSessionHandler -= MediaSessionHandler;
				}
				media_model = value;
				
				if (media_model != null) {
					//ALP FIXME media_model.StreamedMedia.NewMediaSessionHandler += MediaSessionHandler;
					Visible = true;
					//List<DBus.Struct3<uint, DBus.ObjectPath, string>> handlers = media_model.StreamedMedia.GetSessionHandlers ();
					//foreach (DBus.Struct3<uint, DBus.ObjectPath, string> handler in handlers)
					//	MediaSessionHandler (handler.Value1, handler.Value2, handler.Value3);
				}
			}
		}
		
		public new bool Visible
		{
			get { return base.Visible; }
			set
			{
				if (!value)
					Model = null;
				base.Visible = value;
			}
		}
		
		public ContactModel ContactModel
		{
			get { return contact_model; }
			set { contact_model = value; }
		}
		
		void MediaSessionHandler (uint member, NDesk.DBus.ObjectPath session_handler, string type)
		{
			
		}*/
	}
	
	public class DialButton : Gtk.Button
	{
		/*Label label = new Label ();
		Image image = new Image ();
		
		public enum State {
			Disconnected = 1,
			Dialing = 2,
			Connected = 3,
			Error = 4
		}
		
		string [] image_name = new string [] { "DialButton_Disconnected.png", "DialButton_Dialing.png", "DialButton_Connected.png", "DialButton_Error.png" };
		
		StreamedMediaModel media_model;
		State state;
		
		
		public DialButton ()
		{
			HBox box = new HBox ();
			box.Add (image);
			box.Add (label);
			box.Spacing = 5;
			label.NoShowAll = true;
			
			Add (box);
			
			DialState = State.Disconnected;
		}
		
		public StreamedMediaModel Model
		{
			get { return media_model; }
			set
			{
				media_model = value;
				//DialState = State.Disconnected;
			}
		}
		
		public State DialState
		{
			get { return state; }
			set
			{
				if (state != value) {
					state = value;
				
					image.Pixbuf = Global.ImageCache.GetPixbuf (image_name [(int) state - 1]);
					
					switch (state) {
						case State.Dialing:
							label.Text = "Dialing...";
							label.Show ();
							break;
						case State.Error:
							label.Text = "A problem!";
							label.Show ();
							break;
						default:
							label.Hide ();
							break;
					}
				}
			}
		}
		
		
		/*
		protected override void OnClicked ()
		{
			if (state == State.Disconnected) {
				DialState = State.Dialing;
				return;
			}
			
			if (state == State.Error) {
				return;
			}
			
			DialState = State.Disconnected;
		}
		
		public event EventHandler Connect;
		public event EventHandler Disconnect;
		*/
	}
	
	public class SoundButton : Gtk.Button
	{
		VolumeMeter input = new VolumeMeter ();
		VolumeMeter output = new VolumeMeter ();
		
		HBox box = new HBox ();
		
		
		public SoundButton ()
		{
			input.WidthRequest = 25;
			input.Volume = 0.6f;
			input.Color = new Cairo.Color (0.5, 0.9, 0.5);
			
			output.WidthRequest = 25;
			output.Volume = 0.1f;
			output.Color = new Cairo.Color (0.5, 0.5, 0.9);
			
			box.Add (input);
			box.Add (output);
			box.Spacing = 5;

			//box.PackStart (new Label ("Mic"), false, false, 0);
			//box.PackStart (meter, false, false, 0);
			
			//box.Spacing = 5;
			//Add (box);
			Add (box);
			
			GLib.Timeout.Add (50, Timeout);
		}
		
		override protected void OnClicked ()
		{
			input.Mute = !input.Mute;
			output.Mute = !output.Mute;
		}
		
		bool up1, up2;
		
		public bool Timeout ()
		{
			if (up1)
			{
				input.Volume += 0.05f;
				if (input.Volume >= 1)
					up1 = false;
			} else {
				input.Volume -= 0.05f;
				if (input.Volume <= 0)
					up1 = true;
			}
			
			if (up2)
			{
				output.Volume += 0.05f;
				if (output.Volume >= 1)
					up2 = false;
			} else {
				output.Volume -= 0.05f;
				if (output.Volume <= 0)
					up2 = true;
			}
			return true;
		}	
		
	}
	
	public class VolumeMeter : CairoDrawingArea
	{
		float volume = 0;
		bool mute = false;
		Cairo.Color color = new Cairo.Color (1, 1, 1);
		
		public float Volume
		{
			get { return volume; }
			set { volume = value; QueueDraw (); }
		}
		
		public Cairo.Color Color
		{
			get { return color; }
			set { color = value; QueueDraw (); }
		}
		
		public bool Mute
		{
			get { return mute; }
			set { mute = value; QueueDraw (); }
		}
		
		protected override void Draw (Cairo.Context g)
		{
			int width = Allocation.Width;
			int height = Allocation.Height;
			
			double bar_width = (width - 8) / 5;
			double bar_width_inc = 2;
			double bar_height_inc = (height - 5) / 5;
			
			double x = 1;
			double y = 5.5;
			
			g.LineWidth = 0.5;
			
			if (mute) {
				for (double i = 0; i < 5 * volume; ++i)
				{
					g.Rectangle (x, height - y, bar_width, y);
					g.Color = new Cairo.Color (0.8, 0.3, 0.3);
					g.FillPreserve ();
					g.Color = new Cairo.Color (0.2, 0.2, 0.2, 0.6);
					g.Stroke ();
					x += bar_width_inc + bar_width;
				}
			} else {
				for (double i = 0; i < 5 * volume; ++i)
				{
					g.Rectangle (x, height - y, bar_width, y);
					g.Color = color;
					g.FillPreserve ();
					g.Color = new Cairo.Color (0.2, 0.2, 0.2, 0.6);
					g.Stroke ();
					x += bar_width_inc + bar_width;
					y += bar_height_inc;
				}
			}
		}
	}
}
