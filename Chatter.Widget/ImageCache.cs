using System;
using System.Collections.Generic;
using System.Reflection;

namespace Chatter.Widget
{
	public enum ImageSize {
		Big = 32,
		Medium = 24,
		Small = 16,
		NoScale = 0
	}
	
	public class ImageCache
	{
		static Dictionary<string, WeakReference> cache = new Dictionary<string, WeakReference> ();
		
		string Hash (string name, ImageSize size)
		{
			return name + "_size_" + size.ToString ();
		}
		
		public Gdk.Pixbuf GetPixbufPresence (string presence, ImageSize size)
		{
			switch (presence) {
				case Telepathy.PresenceStr.Offline:
				case Telepathy.PresenceStr.Hidden:
					return GetPixbuf ("PresenceImage_Offline.svg", size);
				case Telepathy.PresenceStr.Brb:
					return GetPixbuf ("PresenceImage_Brb.svg", size);
				case Telepathy.PresenceStr.Busy:
					return GetPixbuf ("PresenceImage_Busy.svg", size);
				case Telepathy.PresenceStr.ExtendedAway:
					return GetPixbuf ("PresenceImage_XA.svg", size);
				case Telepathy.PresenceStr.Dnd:
					return GetPixbuf ("PresenceImage_Dnd.svg", size); 
				case "chat":
					return GetPixbuf ("PresenceImage_Chat.svg", size);
				case "away":
					return GetPixbuf ("PresenceImage_Away.svg", size);
				case Telepathy.PresenceStr.Available:
				default:
					return GetPixbuf ("PresenceImage_Online.svg", size);
			}
		}
				
		public Gdk.Pixbuf GetPixbuf (string name, ImageSize size)
		{
			Gdk.Pixbuf pixbuf;
			string hash = Hash (name, size);
			
			if (cache.ContainsKey (hash)) {
				pixbuf = (Gdk.Pixbuf) cache [hash].Target;
				if (pixbuf != null)
					return pixbuf;
			}
			
			if (name.EndsWith ("svg"))
				pixbuf = Rsvg.Pixbuf.LoadFromResource (name);
			else
				pixbuf = new Gdk.Pixbuf (null, name);
			
			if (size != ImageSize.NoScale)
				pixbuf = pixbuf.ScaleSimple ((int) size, (int) size, Gdk.InterpType.Bilinear);
			cache [hash] = new WeakReference (pixbuf);
			return pixbuf;
		}
		
		public Gdk.Pixbuf GetPixbuf (string name)
		{
			return GetPixbuf (name, ImageSize.NoScale);
		}
		
		public Gtk.Image GetImage (string name, ImageSize size)
		{
			return new Gtk.Image (GetPixbuf (name, size));
		}
		
		public Gtk.Image GetImage (string name)
		{
			return GetImage (name, ImageSize.NoScale);
		}
		
		/*public Cairo.ImageSurface GetSurface (string name)
		{
			System.IO.FileStream stream = Assembly.GetCallingAssembly ().GetFile (name);
			System.IO.StreamReader reader = new System.IO.StreamReader (stream);
			string data = reader.ReadToEnd ();
			
			return new Cairo.ImageSurface (data, );
		}*/
		
	}

}
