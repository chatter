using System;
using System.IO;
using Gtk;
using Cairo;

namespace Chatter.Widget
{
	public class VideoComponent : HBox
	{
		public VideoComponent ()
		{
			
		}
		
		ContactModel contact_model;
				
		public void ForgetModel () { }
		
		public ContactModel ContactModel
		{
			get { return contact_model; }
			set { contact_model = value; }
		}
	}
	
	public class VideoOut : CairoDrawingArea
	{
		public VideoOut ()
		{
			HeightRequest = 100;
			WidthRequest = 200;
			//Allocation = new Gdk.Rectangle (0, 0, 100,100);
			//Allocation.X = 100;
			//Allocation.Y = 100;
		}
		
		protected override void Draw (Cairo.Context g)
		{
			g.Color = new Color (0, 0, 0);
			g.Paint ();
		}
	}
}
