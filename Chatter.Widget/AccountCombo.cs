using System;
using System.Collections.Generic;
using Gtk;
using NDesk.DBus;

namespace Chatter.Widget
{
	public class AccountCombo : ComboBox
	{
		ListStore store = new ListStore (typeof (AccountModel));
		AccountStore account_store;
		
		public AccountCombo (AccountStore _account_store) : base ()
		{
			InitUI ();
			
			account_store = _account_store;
			account_store.AccountAdded += AccountAddedHandler;
			account_store.AccountRemoved += AccountRemovedHandler;
			
			foreach (AccountModel account in account_store.Accounts)
				AccountAddedHandler (account);
		}
		
		void InitUI ()
		{
			CellRendererText text_render = new CellRendererText ();
			PackStart (text_render, true);
			SetCellDataFunc (text_render, CellLayoutText);
			
			Model = store;
		}
		
		public void AccountAddedHandler (AccountModel account)
		{
			TreeIter it = store.AppendValues (account);
			SetActiveIter (it);
		}
		
		public void AccountRemovedHandler (AccountModel account)
		{
			TreeIter it;
			store.GetIterFirst (out it);
			AccountModel it_account;
			while (store.IterIsValid (it)) {
				it_account = (AccountModel) store.GetValue (it, 0);
				if (it_account == account)
					store.Remove (ref it);
				else
					store.IterNext (ref it);
			}

			store.GetIterFirst (out it);
			if (store.IterIsValid (it))
				SetActiveIter (it);
			else
				Active = -1;
				
		}
		
		public AccountModel Selected
		{
			get
			{
				TreeIter it;
				if (GetActiveIter (out it))
					return (AccountModel) store.GetValue (it, 0);
				
				return null;
			}
		}
		
		void CellLayoutText (CellLayout cell_layout, CellRenderer cell_render, TreeModel model, TreeIter it)
		{
			CellRendererText text_render = (CellRendererText) cell_render;
			AccountModel account = (AccountModel) model.GetValue (it, 0);
			
			if (account != null)
				text_render.Text = "[" + account.Protocol + "] " + account.AccountStr;
		}
	}
}

