using System;
using System.Collections.Generic;
using Gtk;

namespace Chatter.Widget
{
	public delegate void ButtonPressDelegate ();
	
	public class ChatterIcon : NotificationArea
	{
		EventBox event_box = new EventBox ();
		Image image = new Image ();
		
		public ChatterIcon () : base ("Chatter")
		{
			event_box.Add (image);
			
			Add (event_box);
			event_box.ShowAll ();
		}
		
		public event ButtonPressDelegate ButtonPress;
		
		override protected bool OnButtonPressEvent (Gdk.EventButton args)
		{
			if (ButtonPress != null) ButtonPress ();
			return true;
		}
	}
}
