using System;
using System.IO;
using Gtk;
using Telepathy;

namespace Chatter.Widget
{
	public delegate void NotifyItemCloseDelegate (NotifyItem item);
	
	public class NotifyItem : EventBox
	{
		public void Close ()
		{
			if (Closed != null) Closed (this);
		}
		
		public event NotifyItemCloseDelegate Closed;

		protected override bool OnExposeEvent (Gdk.EventExpose args)
		{
			Gdk.Window win = args.Window;
			       
			using (Cairo.Context g = Gdk.Context.CreateDrawable (win, true))
			{
				Draw (g);
			}
			
			foreach (Gtk.Widget child in Children)
				PropagateExpose (child, args);
			
			return true;
        }
        
		protected virtual void Draw (Cairo.Context g)
		{
			double width = Allocation.Width;
        	double height = Allocation.Height;
        	
			g.Color = new Cairo.Color (0.8, 0.8, 0.9);
			CairoDrawingArea.RectangleCurve (g, 3, 3, width - 6, height - 6, 5);
			g.FillPreserve ();
			g.Color = new Cairo.Color (0.5, 0.5, 0.6);
			g.Stroke ();
		}
	}
	
	public class AccountNotify : NotifyItem
	{
		AccountModel model;
		
		VBox top_vbox = new VBox ();
		HBox top_hbox = new HBox ();
		HBox bottom_hbox = new HBox ();	
			
		Label top_label = new Label ();
		Button close = new Button ();
		
		Label status_label = new Label ();
		
		public AccountNotify (AccountModel account_model, ConnectionModel connection_model)
		{		
			InitUI ();
			
			if (connection_model.Connection != null)
			{
				connection_model.Connection.StatusChanged += OnStatusChanged;
				OnStatusChanged (connection_model.Connection.Status, ConnectionStatusReason.NoneSpecified);
			}
			else
			{
				OnStatusChanged (ConnectionStatus.Disconnected, ConnectionStatusReason.NoneSpecified);
			}
		}
		
		void InitUI ()
		{
			top_label.Justify = Justification.Left;
			top_hbox.PackStart (top_label, true, true, 0);
			close.Image = new Image (Stock.Close, IconSize.Menu);
			top_hbox.PackStart (close, false, true, 0);
			top_vbox.Add (top_hbox);
			
			bottom_hbox.PackStart (status_label, true, true, 0);
			top_vbox.Add (bottom_hbox);
			
			top_vbox.Spacing = 3;
			top_vbox.BorderWidth = 8;
			Add (top_vbox);
			top_vbox.ShowAll ();
			close.Clicked += CloseClickedHandler;
		}

		void OnStatusChanged (ConnectionStatus status, ConnectionStatusReason reason)
		{
			switch (status) {
				case ConnectionStatus.Connected:
					Close ();
					break;
				case ConnectionStatus.Connecting:
					status_label.Text = "Connecting";
					break;
				case ConnectionStatus.Disconnected:
					string further_text = String.Empty;
					switch (reason) {
						case ConnectionStatusReason.Requested:
							Close ();
							return;
						
						case ConnectionStatusReason.NoneSpecified:
							further_text = "Unknown reason";
							break;
						case ConnectionStatusReason.NetworkError:
							further_text = "There was a network error";
							break;
						case ConnectionStatusReason.AuthenticationFailed:
							further_text = "Authentication has failed";
							break;
						case ConnectionStatusReason.EncryptionError:
							further_text = "There was a problem with the encryption";
							break;
					}
					status_label.Text = "Error in connection: " + further_text;
					break;
				default:
					status_label.Text = "Unknown " + status.ToString ();
					break;
			}
		}
			
		void CloseClickedHandler (object o, EventArgs args)
		{
			Close ();
		}
	}
	
	public class LabelNotify : NotifyItem
	{
		HBox hbox = new HBox ();
		Label top_label = new Label ();
		Button close = new Button ();
		
		public LabelNotify (string text)
		{
			top_label.Markup = text;
			top_label.SingleLineMode = false;
			top_label.LineWrap = false;
			top_label.Wrap = true;

			hbox.PackStart (top_label);
			close.Image = new Image (Stock.Close, IconSize.Menu);
			hbox.PackStart (close);
			
			hbox.BorderWidth = 8;
			hbox.Spacing = 3;
			Add (hbox);
			hbox.ShowAll ();
			close.Clicked += delegate (object o, EventArgs args) { Close (); };
		}
	}
}
