all: Phone.exe

RESOURCE= -resource:data/phone.glade,phone.glade \
	-resource:data/buddy.png,buddy.png \
	-resource:data/buddy2.png,buddy2.png \
	-resource:data/DialButton_Disconnected.png,DialButton_Disconnected.png \
	-resource:data/DialButton_Connected.png,DialButton_Connected.png \
	-resource:data/DialButton_Dialing.png,DialButton_Dialing.png \
	-resource:data/DialButton_Error.png,DialButton_Error.png \
	-resource:data/SmileButton_smile.png,SmileButton_smile.png \
	-resource:data/ActionCombo_Action.png,ActionCombo_Action.png \
	-resource:data/PresenceImage_Away.svg,PresenceImage_Away.svg \
	-resource:data/PresenceImage_Brb.svg,PresenceImage_Brb.svg \
	-resource:data/PresenceImage_Dnd.svg,PresenceImage_Dnd.svg \
	-resource:data/PresenceImage_Offline.svg,PresenceImage_Offline.svg \
	-resource:data/PresenceImage_Chat.svg,PresenceImage_Chat.svg \
	-resource:data/PresenceImage_Online.svg,PresenceImage_Online.svg \
	-resource:data/PresenceImage_XA.svg,PresenceImage_XA.svg
	
Phone.exe: *.cs Chatter.Widget/*.cs Chatter.Plugin/Plugin.cs
	gmcs -debug *.cs Chatter.Widget/*.cs Chatter.Plugin/Plugin.cs -out:Phone.exe $(RESOURCE) -pkg:glade-sharp-2.0 -r:Mono.Cairo -r:NDesk.DBus.dll -r:NDesk.DBus.GLib.dll -r:INdT.Telepathy.dll -r:Mono.Posix -pkg:rsvg-sharp-2.0

clean:
	rm -f Phone.exe
